const express = require('express');
const dotenv = require('dotenv').config()
const app = express();
const port = process.env.PORT || 3000;

app.use(express.json())

app.get('/', (req,res) => {
   
    res.json(req.headers)
})

app.listen(port, () => console.log(`Running on port ${port}`))
