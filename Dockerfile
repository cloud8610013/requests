FROM node:20.10.0-alpine3.18
ARG PROFILE
RUN apt-get update \;
    apt-get install -y pm2 \;
    mkdir -p /requests \;
    

COPY app/ /requests/
WORKDIR /requests
ENTRYPOINT ["pm2","ecosystem.config.js"]
